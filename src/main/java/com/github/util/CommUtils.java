package com.github.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Repository
public class CommUtils
{
    @Autowired
    private QuestionUtil questionUtil;
    @Value("${selectQuestionNums}")
    private int selectQuestionNums;
    /**
     * 根据传入数据，得到不重复的题号
     * @param usedQuestions
     * @param degree
     * @return
     */
    public Set<Integer> generateRandomQuestions(Set<Integer> usedQuestions, Integer degree)
    {
        //查询当前等级的题号范围
        Map<String,Integer> numMap = questionUtil.queryEveryDegreeMaxAndMin().get(degree);
        int max = numMap.get("max");
        int min = numMap.get("min");
        Set<Integer> paper = new HashSet<>();
        if (usedQuestions != null && usedQuestions.size() > 0)
        {
            int usedQuestionNums = usedQuestions.size();
            for (int i = usedQuestions.size(); i < usedQuestionNums + selectQuestionNums ; )
            {
                int select  = (int)(Math.random()*(max - min + 1))+min;
                usedQuestions.add(select);
                if (usedQuestions.size() > i)
                {
                    paper.add(select);
                    i = usedQuestions.size();
                }

            }
        }
        else
        {
            for (int i = 0;i < selectQuestionNums ; )
            {
                int select  = (int)(Math.random()*(max - min + 1))+min;
                paper.add(select);
                i = paper.size();
            }
        }
        return paper;
    }
}
