package com.github.util;

import com.github.model.UserSessionInfo;
import com.github.service.UserService;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author xiaohelin
 * @create 2017-12-06 11:28
 **/
@Component
public class SessionInfoArgumentResolver implements HandlerMethodArgumentResolver {

    @Resource
    private UserService userService;

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().equals(UserSessionInfo.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        HttpSession session = request.getSession();

        String sessionName = "userSessionInfo";
        Object userSessionInfo = webRequest.getAttribute(sessionName, RequestAttributes.SCOPE_SESSION);
        if (userSessionInfo == null) {
            userSessionInfo = new UserSessionInfo();

            //33864113 22577580
            String uid = "22577580";
            userSessionInfo = userService.getUserInfo(uid);
            //从cookie
//            Cookie cookie = WebUtils.getCookie(request, Constants.UID);
//            if(cookie!=null){
//                String uid = cookie.getValue();
//                userSessionInfo = userService.getUserInfo(uid);
//                session.setAttribute(sessionName, userSessionInfo);
//            }else{
//                userSessionInfo = null;
//            }
        }else{

            Cookie cookie = WebUtils.getCookie(request, Constants.UID);
            if(cookie == null){
                session.removeAttribute(sessionName);
                userSessionInfo = null;
            }else{
                String uid = cookie.getValue();
                String sessionUid = ((UserSessionInfo)userSessionInfo).getUid();
                if(!uid.equals(sessionUid)){
                    userSessionInfo = userService.getUserInfo(uid);
                    session.setAttribute(sessionName, userSessionInfo);
                }
            }
        }
        return userSessionInfo;
    }
}
