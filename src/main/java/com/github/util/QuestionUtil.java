package com.github.util;

import com.github.mapper.WmQuestionMapper;
import com.github.vo.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class QuestionUtil
{
    @Autowired
    private WmQuestionMapper questionMapper;
    /**
     * 缓存题目
     * @return
     */
    @Cacheable(value="question")
    public Map<Integer,Question> queryQuestion()
    {
        //查询所有的题目
        System.out.println("查询所有题目");
        List<Question> questions =  questionMapper.selectAllQuestions();
        Map<Integer,Question> questionMap = new HashMap<>();
        for (Question question : questions)
        {
            questionMap.put(question.getQuestionId(),question);
        }
        return questionMap;
    }

    /**
     * 缓存各个等级的最大最小值
     */
    @Cacheable(value="degreeMap")
    public Map<Integer,Map<String,Integer>> queryEveryDegreeMaxAndMin()
    {
        System.out.println("查询最大最小");
        List<Map<String,Integer>> nums = questionMapper.selectMaxAndMinNums();
        Map<Integer,Map<String,Integer>> degreeMap = new HashMap<>();
        for (Map<String,Integer> map : nums)
        {
            Integer degree = map.get("degree");
            Map<String,Integer> maxAndmin = new HashMap<>();
            maxAndmin.put("max",map.get("max"));
            maxAndmin.put("min",map.get("min"));
            degreeMap.put(degree,maxAndmin);
        }
        return degreeMap;
    }
}
