package com.github.mapper;

import com.github.vo.Question;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface WmQuestionMapper {
    /**
     * 查询所有题目
     */
    List<Question> selectAllQuestions();

    /**
     * 查询一个等级的最大最小问题号，用逗号分割
     */
    List<Map<String,Integer>>  selectMaxAndMinNums();
}