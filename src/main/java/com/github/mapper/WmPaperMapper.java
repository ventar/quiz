package com.github.mapper;

import com.github.model.WmPaper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface WmPaperMapper
{
    /**
     * 插入试卷信息
     * @param paper
     */
    void insertPaperData(WmPaper paper);

    /**
     * 如果插入错误,删除paper
     * @param paperId
     */
    void deleteDataByPaperId (@Param("paperId")int paperId);

    /**
     * 查询上次使用过的题目
     * @param uid
     * @return
     */
    List<Integer> selectUsedQuestions(@Param("uid") String uid);

    /**
     * 更新试卷信息
     * @param paperId
     * @param number
     * @param score
     */
    void updatePaper (@Param("paperId") int paperId,@Param("number") int number, @Param("score") int score);

    /**
     * 查询答题结果
     * @param uid
     * @return
     */
    Integer selectPaperScore(@Param("uid") String uid);
}