package com.github.mapper;

import com.github.model.WmUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface WmUserMapper
{
    WmUser selectUserDetail(@Param("uid")String uid);

    /**
     *为防止中途退出先机会减少1
     * @param uid
     */
    void updateUserDeleteChance (@Param("uid")String uid);

    /**
     * 过关
     * @param uid
     */
    void updateUserData (@Param("uid")String uid);

    /**
     * 第二天重置为2次机会
     */
    void updateResetUserChance();

    /**
     * 查询选手通关名次
     * @param uid
     * @return
     */
    String selectUserRanking(@Param("uid") String uid);

    /**
     * 新增用户信息
     * @param uid
     * @param name
     * @param fid
     */
    void insertUser (@Param("uid") String uid,@Param("name") String name,@Param("fid") String fid);
}