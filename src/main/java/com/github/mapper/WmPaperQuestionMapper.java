package com.github.mapper;

import com.github.model.WmPaperQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface WmPaperQuestionMapper
{
    /**
     * 插入试卷所有问题
     * @param paperData
     */
    void  insertPaperQuestionsData(Map<String,Object> paperData);

    /**
     *一条更新所有数据
     */
    void updateQuestionAnswers(@Param("questions") List<WmPaperQuestion> questions, @Param("paperId")Integer paperId);
}