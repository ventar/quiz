package com.github.controller;

import com.github.model.UserSessionInfo;
import com.github.service.WmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Controller
@RequestMapping
public class WmController
{
    @Autowired
    private WmService wmService;

    @GetMapping({"", "/"})
    public String index() {
        return UrlBasedViewResolver.FORWARD_URL_PREFIX + "/index.html";
    }


    /**
     * 登陆
     *
     */
    @RequestMapping("main")
    public String main(UserSessionInfo userSessionInfo, Model model)
    {
        wmService.loginCompetition(userSessionInfo,model);
        return "main";
    }

    /**
     *答题
     */
    @RequestMapping("competition")
    public String startContest(UserSessionInfo userSessionInfo, Model model)
    {
        String uid = userSessionInfo.getUid();
        wmService.startCompetition(uid,model);
        return "answer";
    }

    /**
     * 答题结果
     * @return
     */
    @RequestMapping("result")
    public String queryExamResult(UserSessionInfo userSessionInfo, Model model)
    {
        String uid = userSessionInfo.getUid();
        wmService.getResultData(uid,model);
        return  "result";
    }
}
