package com.github.controller;

import com.alibaba.fastjson.JSON;
import com.github.model.UserSessionInfo;
import com.github.service.WmAjaxService;
import com.github.vo.Paper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping
public class WmAjaxController
{
    @Autowired
    private WmAjaxService ajaxService;
    @Value("${competitionEndTime}")
    private long competitionEndTime;
    /**
     * 生成试卷
     */
    @RequestMapping("getPaperInfoByAjax")
    public Object getPaperInfoByAjax(UserSessionInfo userSessionInfo)
    {
        long nowTime = new Date().getTime();
        Map<String,String> returnData = new HashMap<>();
        if (nowTime < competitionEndTime)
        {
            String uid = userSessionInfo.getUid();
            Paper paper = ajaxService.queryPaper(uid);
            if (paper != null)
            {
                returnData.put("code","0");
                returnData.put("paper",JSON.toJSONString(paper));
            }
            else
            {
                returnData.put("code","1");
                returnData.put("msg","生成题目失败，请重试");
            }
        }
        else
        {
            returnData.put("code","1");
            returnData.put("msg","博物馆读书节活动已结束");
        }

        return returnData;
    }

    /**
     * 提交试卷
     */
    @RequestMapping("subPaperInfoByAjax")
    public Object subPaperInfoByAjax(UserSessionInfo userSessionInfo, @RequestParam("iqPaperUserId") Integer paperId, @RequestParam("answerJson") String answerJson){
        long nowTime = new Date().getTime();
        Map<String,String> returnData = new HashMap<>();
        if (nowTime > competitionEndTime)
        {
            String uid = userSessionInfo.getUid();
            Map<String,String> answers = (Map<String, String>) JSON.parse(answerJson);
            ajaxService.sumPaperAnswer(paperId,answers,uid);
            returnData.put("code","0");
        }
        else
        {
            returnData.put("code","1");
            returnData.put("msg","博物馆读书节活动已结束");
        }
        return returnData;
    }
}
