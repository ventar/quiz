package com.github.service;

import com.github.mapper.WmPaperMapper;
import com.github.mapper.WmPaperQuestionMapper;
import com.github.mapper.WmUserMapper;
import com.github.model.WmPaper;
import com.github.model.WmPaperQuestion;
import com.github.model.WmQuestionOption;
import com.github.model.WmUser;
import com.github.util.CommUtils;
import com.github.util.QuestionUtil;
import com.github.vo.Paper;
import com.github.vo.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WmAjaxService
{
    @Autowired
    private WmUserMapper userMapper;
    @Autowired
    private WmPaperMapper paperMapper;
    @Autowired
    private WmPaperQuestionMapper paperQuestionMapper;
    @Autowired
    private QuestionUtil  questionUtil;
    @Autowired
    private CommUtils commUtils;
    @Value("${correctQuestionNums}")
    private int correctQuestionNums;
    /**
     * 查询试卷
     * @param uid
     * @return
     */
    public Paper queryPaper(String uid)
    {
        //选择出题号
        WmUser user = userMapper.selectUserDetail(uid);
        int degree = user.getDegree();
        //查询用过的题号
        Set<Integer> usedPaper = new HashSet<>(paperMapper.selectUsedQuestions(uid));
        Set<Integer> paperQuestionNums = commUtils.generateRandomQuestions(usedPaper,degree);
        //生成试卷,如果报错直接返回错误信息
        int paperId = -1;
        try
        {
            //插入试卷信息
            WmPaper paper = new WmPaper();
            paper.setQuestionNumber(paperQuestionNums.size());
            paper.setUid(uid);
            paperMapper.insertPaperData(paper);
            paperId =  paper.getPaperId();
            //插入问题
            Map<String,Object> map = new HashMap<>();
            map.put("paperId",paperId);
            map.put("questions",paperQuestionNums);
            paperQuestionMapper.insertPaperQuestionsData(map);
        }
        catch (Exception e)
        {
            if (paperId == -1) {
                //删除相应的试卷记录
                paperMapper.deleteDataByPaperId(paperId);
                return null;
            }
        }
        // 查出试卷
        Paper paper = new Paper();
        paper.setPaperId(paperId);
        List<Question> questions = new ArrayList<>();
        Map<Integer, Question> questionMap = questionUtil.queryQuestion();

        for (Integer ques : paperQuestionNums) {
            questions.add(questionMap.get(ques));
        }
        paper.setQuestions(questions);
        //先减少机会
        userMapper.updateUserDeleteChance(uid);
        return paper;
    }

    /**
     * 判定答题结果
     * @param paperId
     * @param questions
     * @return
     */
    public void sumPaperAnswer(int paperId,Map<String,String> questions,String uid)
    {
        int correctNum = 0;
        Map<Integer,Question> questionMap = questionUtil.queryQuestion();
        System.out.println("题目总数"+questionMap.keySet().size());
        List<WmPaperQuestion> userQuestions = new ArrayList<>();
        for (String question : questions.keySet())
        {
            int ques = Integer.parseInt(question);
            String answer = questions.get(question);
            //计算正确情况
            List<WmQuestionOption> questionOptions = questionMap.get(ques).getOptions();
            Integer correctAnswer = 0;
            WmPaperQuestion  paperQuestion = new WmPaperQuestion(paperId,ques,answer);
            userQuestions.add(paperQuestion);
            for (WmQuestionOption choose:questionOptions)
            {
                if ("1".equals(choose.getIsReal()))
                {
                    correctAnswer = choose.getChooseId();
                    break;
                }
            }

            if (answer.equals(correctAnswer.toString()))
            {
                correctNum++;
            }
        }
        //更新答案
        paperQuestionMapper.updateQuestionAnswers(userQuestions,paperId);
        int number = questions.keySet().size();
        //提交后更新试卷信息
        paperMapper.updatePaper(paperId,number,correctNum);

        if (correctNum >= correctQuestionNums)
        {
            userMapper.updateUserData(uid);
        }
    }
}
