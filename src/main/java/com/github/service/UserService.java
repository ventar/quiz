package com.github.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.model.UserSessionInfo;
import com.github.util.HttpClientJdk;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Cacheable(value="objectCache", key="#root.targetClass.getSimpleName().concat(':').concat(#root.methodName).concat(':').concat(#uid)")
    public UserSessionInfo getUserInfo(String uid) {
        String requestUrl = "http://mooc1-api.chaoxing.com/gas/person?userid={uid}&fields=group1,schoolid,roleids,loginname,username".replace("{uid}", uid.toString());
        String responseContent = HttpClientJdk.httpRequestGet(requestUrl);
        JSONObject jsonObject = JSON.parseObject(responseContent).getJSONArray("data").getJSONObject(0);
        UserSessionInfo userSessionInfo = new UserSessionInfo();
        userSessionInfo.setUid(uid);
        userSessionInfo.setRealName(jsonObject.getString("username"));
        userSessionInfo.setFid(jsonObject.getString("schoolid"));
        return userSessionInfo;
    }
}