package com.github.service;

import com.github.mapper.WmPaperMapper;
import com.github.mapper.WmQuestionMapper;
import com.github.mapper.WmUserMapper;
import com.github.model.UserSessionInfo;
import com.github.model.WmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Date;

@Service
public class WmService
{
    @Autowired
    private WmQuestionMapper questionMapper;
    @Autowired
    private WmPaperMapper paperMapper;
    @Autowired
    private WmUserMapper userMapper;
    @Value("${competitionEndTime}")
    private long competitionEndTime;
    @Value("${competitionStartTime}")
    private long competitionStartTime;

    /**
     * 登陆
     * @param userSessionInfo
     * @param model
     */
    public void loginCompetition(UserSessionInfo userSessionInfo, Model model)
    {
        String uid = userSessionInfo.getUid();
        if (uid != null)
        {
            WmUser user = userMapper.selectUserDetail(uid);
            if (user != null)
            {
                int degree = user.getDegree();
                if (degree < 5)
                {
                    long now = new Date().getTime();
                    if (now < competitionEndTime)
                    {
                        int chance = user.getChance();
                        model.addAttribute("chance",chance);
                        //其次，判断机会是否已用完和下一关是否开启
                        judgeChanceAndLevel(chance,degree,now,model);
                    }
                    else
                    {
                        model.addAttribute("code",1);
                        model.addAttribute("msg","博物馆知识竞赛已结束");
                    }
                }
                else
                {
                    String ranking = userMapper.selectUserRanking(uid);
                    model.addAttribute("code",1);
                    model.addAttribute("ranking",ranking);
                }
            }
            else
            {
                long now = new Date().getTime();
                if (now < competitionEndTime)
                {
                    //插入信息
                    String fid = userSessionInfo.getFid();
                    String name = userSessionInfo.getRealName();
                    userMapper.insertUser(uid,name,fid);
                    if (now > competitionStartTime)
                    {
                        model.addAttribute("code",0);
                        model.addAttribute("degree",0);
                        model.addAttribute("chance",2);
                    }
                    else
                    {
                        model.addAttribute("code",1);
                        model.addAttribute("msg","博物馆知识竞赛还未开始");
                    }
                }
                else
                {
                    model.addAttribute("code",1);
                    model.addAttribute("msg","博物馆知识竞赛已结束");
                }
            }
        }
        else
        {
            model.addAttribute("code",1);
            model.addAttribute("msg","账号出错，请重新登陆");
        }
    }

    /**
     * 开始答题信息
     * @param uid
     * @param model
     */
    public void startCompetition(String uid,Model model)
    {
        //首先，判断是否已经结束
        long now = new Date().getTime();
        if (now < competitionEndTime)
        {
            WmUser user = userMapper.selectUserDetail(uid);
            int degree = user.getDegree();
            int chance = user.getChance();
            //其次，判断机会是否已用完和下一关是否开启
            judgeChanceAndLevel(chance,degree,now,model);
        }
        else
        {
            model.addAttribute("code",1);
            model.addAttribute("msg","博物馆知识竞赛已结束");
        }

    }

       /**
     * 得到试卷结果
     * @param uid
     * @param model
     * @return
     */
    public void getResultData (String uid,Model model)
    {
        int score = paperMapper.selectPaperScore(uid);
        WmUser user = userMapper.selectUserDetail(uid);
        //首先，判断答题成功还是失败
        if (score >= 8)
        {
            model.addAttribute("result",0);
            int degree = user.getDegree();
            model.addAttribute("degree",degree);
            //再次，判断是否已经全部通关
            if (degree == 5)
            {
                model.addAttribute("code",1);
            }
            else
            {
                int chance =  user.getChance();
                long now = new Date().getTime();
                //判断是否截止
                if (now < competitionEndTime)
                {
                    //其次，判断机会是否已用完和下一关是否开启
                    judgeChanceAndLevel(chance,degree,now,model);
                }
                else
                {
                    model.addAttribute("code",1);
                    model.addAttribute("msg","博物馆知识竞赛已结束");
                }
            }
        }
        else
        {
            model.addAttribute("result",1);
            long now = new Date().getTime();
            //判断是否截止
            if (now < competitionEndTime)
            {
                int chance = user.getChance();
                model.addAttribute("chance",chance);
                if (chance > 0)
                {
                    model.addAttribute("code",0);
                }
                else
                {
                    model.addAttribute("code",1);
                }
            }
            else
            {
                model.addAttribute("code",1);
                model.addAttribute("msg","博物馆知识竞赛已结束");
            }
        }
    }

    /**
     * 判断机会是否用完和关卡是否开启
     */
    private void judgeChanceAndLevel(int chance,int degree,long now,Model model)
    {
        if (chance > 0 && now > degree*24*60*60*1000+competitionStartTime)
        {
            model.addAttribute("code",0);
        }
        else if (chance <= 0)
        {
            model.addAttribute("code",1);
            model.addAttribute("msg","今日机会已经用完");
        }
        else if (now < competitionStartTime)
        {
            model.addAttribute("code",1);
            model.addAttribute("msg","博物馆知识竞赛还未开始");
        }
        else
        {
            model.addAttribute("code",1);
            model.addAttribute("msg","下关明日9时开启，敬请期待");
        }
        //分两步,首先判断哪些关卡已开启
        for (int i=0;i <= (now-competitionStartTime)/(24*60*60*1000);i++)
        {
            model.addAttribute("class"+(i+1),"state-2-"+(i+1));
        }
        //判断用户通过的关卡
        for (int i=1;i <= degree;i++)
        {
            model.addAttribute("class"+i,"state-1-"+i);
        }
    }
}
