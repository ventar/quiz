package com.github.vo;


import com.github.model.WmQuestionOption;

import java.util.List;

public class Question
{
    /** column: question_id*/
    private Integer questionId;
    /** column: describes*/
    private String content;
    /** column: degree*/
    private Integer degree;
    /** column: answer_time*/
    private Integer answerTime;
    /** column: img_url*/
    private String imgUrl;
    /** column: flag*/
    private Integer flag;

    private List<WmQuestionOption> options;

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }

    public Integer getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(Integer answerTime) {
        this.answerTime = answerTime;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public List<WmQuestionOption> getOptions() {
        return options;
    }

    public void setOptions(List<WmQuestionOption> options) {
        this.options = options;
    }
}
