package com.github.model;

/**
 * 用户session
 *
 * @author xiaohelin
 * @create 2017-12-06 11:27
 **/
public class UserSessionInfo {

    private String uid;
    private String realName;
    private String fid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }
}
