// 页面适应
var winWidth = $(window).width();
var winHeight = $(window).height();

function skipAdaption() {
  $(".shareLink").css({
    "height": winHeight,
    "background": "#f6f7f2"
  });
  $(".skipBg").css({
    "height": winHeight
  });
}

function mainAdaption() {
  $(".tit").css({
    "height": winWidth / 750 * 598
  });
  $(".begin").css({
    "height": winWidth / 750 * 656
  });
  $(".rule").css({
    "height": winWidth / 750 * 581
  });
}

function resultAdaption() {
  $(".result").css({
    "height": winHeight
  });
}

function answerAdaption() {
  $(".topicImg img").css({
    "height": winWidth / 750 * 380
  });
}

// 选择样式修改
function answerSelect() {
  $(".topicOptions>li").on("click", function () {
    if (!$(this).hasClass('selected')) {
      $(this).addClass('selected').siblings().removeClass('selected');
    }
  })
}