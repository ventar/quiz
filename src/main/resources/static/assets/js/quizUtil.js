var QuizUtil = {
  knowledgeName: "博物馆知识竞赛",
  downloadurl: "https://app.chaoxing.com",

  //首页分享
  shareTitle: '博物馆知识竞赛',
  shareLogo: '../image/icon_share.png',
  shareUrl: '../../index.html',
  resUid: 'd12dc83c480e2f862189f013313af675',
  share: function () {},

  //好友邀请
  inviteTitle: '博物馆知识竞赛',
  inviteLogo: '../image/icon_invite.png',
  friendInvitationUrl: "https://www.baidu.com",
  friendInvitationUid: "aece9903f74ca927580a9e855ae698da",
  inviteFriend: function () {},

  //战绩分享
  recordShareTitle: '博物馆知识竞赛',
  recordShareLogo: '../image/icon_invite.png',
  recordShareUrl: "https://www.google.com",
  recordShareUid: '6a59545922e5806e8f4c124897e9cd03',
  shareFightResult: function () {},

  openApp: function () {},
  getIOSVersion: function () {},
  loginApp: function (callback) {},

  circleId: 2238431,
  exitApp: function () {
    addStorageData("quize_main", "main");
    AppUtils.closeView();
  },
  closeView: function () {
    var val = $.trim(getStorageVal("quize_main"));
    if (val == "main") {
      delStorageVal("quize_main");
      AppUtils.closeView();
    }
  }
}
QuizUtil.share = function (customShareLogo, customShareTitle, customShareUrl) {

  var logo = this.shareLogo;
  var title = this.shareTitle;
  var url = this.shareUrl;
  if (undefined != customShareLogo && customShareLogo != '') {
    logo = customShareLogo;
  }
  if (undefined != customShareTitle && customShareTitle != '') {
    title = customShareTitle;
  }
  if (undefined != customShareUrl && customShareUrl != '') {
    url = customShareUrl;
  }
  var content = { "resTitle": title, "resUrl": url, "resLogo": logo, "resUid": this.resUid };

  jsBridge.postNotification("CLIENT_TRANSFER_INFO", { "cataid": 100000015, "content": content });
}

QuizUtil.inviteFriend = function (uuid, challengerPuid, questionType, customInviteLogo, customInviteTitle) {
  //转发网页实例
  var url = QuizUtil.friendInvitationUrl + "?uuid=" + uuid + "&challengerPuid=" + challengerPuid + "&questionType=" + questionType;

  var logo = this.inviteLogo;
  var title = this.inviteTitle;
  if (undefined != customInviteLogo && customInviteLogo != '') {
    logo = customInviteLogo;
  }
  if (undefined != customInviteTitle && customInviteTitle != '') {
    title = customInviteTitle;
  }
  var content = { "resTitle": title, "resUrl": url, "resLogo": logo, "resUid": this.friendInvitationUid };

  jsBridge.postNotification("CLIENT_TRANSFER_INFO", { "cataid": 100000015, "content": content });

}

QuizUtil.shareFightResult = function (uuid, sharerPuid, customRecordShareLogo, customRecordShareTitle) {
  var url = QuizUtil.recordShareUrl + "?uuid=" + uuid + "&sharerPuid=" + sharerPuid;
  var logo = this.recordShareLogo;
  var title = this.recordShareTitle;
  if (undefined != customRecordShareLogo && customRecordShareLogo != '') {
    logo = customRecordShareLogo;
  }
  if (undefined != customRecordShareTitle && customRecordShareTitle != '') {
    title = customRecordShareTitle;
  }
  var content = { "resTitle": title, "resUrl": url, "resLogo": logo, "resUid": this.recordShareUid };
  jsBridge.postNotification("CLIENT_TRANSFER_INFO", { "cataid": 100000015, "content": content });
}

QuizUtil.getIOSVersion = function () {
  var ua = navigator.userAgent.toLowerCase();
  if (ua.match(/(iphone|ipod);?/i)) {
    if (ua.match(/iphone os ([0-9]+)_/) != null) {
      return ua.match(/iphone os ([0-9]+)_/)[1];
    }
  } else if (ua.match(/(ipad);?/i)) {
    if (ua.match(/cpu os ([0-9]+)_/) != null) {
      return (ua.match(/cpu os ([0-9]+)_/))[1];
    }
  }
  return -1;
}

QuizUtil.addIphoneHeight = function () {
  //获取客户端版本号
  if (navigator.userAgent.indexOf("ChaoXingStudy") > -1) {
    var ua = navigator.userAgent;
    ua = ua.substring(ua.indexOf("_") + 1);
    ua = ua.substring(ua.indexOf("_") + 1);
    ua = ua.substring(0, ua.indexOf("_"));
    var version = ua;
    if (version > '3.2.0.6' || version == '3.2.0.6') {
      var isIphone = /iphone/gi.test(navigator.userAgent);
      if (isIphone && (screen.height == 812 && screen.width == 375)) {
        //iphoneX
        $('body').addClass('iosxwrapMax');
        // $('.tzlx .introBox').css("height", "calc(100vh - 8.8rem)");
        // $(".phb .scrollBox").css("height", "calc( 100vh - 5.18rem)");
        // $(".lsph .scrollBox").css("height", "calc(100vh - 3.6rem)");
      } else if (isIphone && (screen.width == 414)) {
        //iphone plus
        $('body').addClass('iospluswrapMax');
        // $('.tzlx .introBox').css("height", "calc(100vh - 8rem)");
        // $(".phb .scrollBox").css("height", "calc( 100vh - 4.9rem)");
        // $(".lsph .scrollBox").css("height", "calc(100vh - 3.1rem)");
      } else if (isIphone) {
        //其他iphone
        $('body').addClass('ioswrapMax');
        // $('.tzlx .introBox').css("height", "calc(100vh - 8rem)");
        // $(".phb .scrollBox").css("height", "calc( 100vh - 4.8rem)");
        // $(".lsph .scrollBox").css("height", "calc(100vh - 3.1rem)");
      }
      //iphone4 5适配
      if ((screen.height <= 568 && screen.width == 320)) {
        // $('.tzlx .intro p.title').css("padding-top", '0');
        // $(".phb .scrollBox").css("height", "calc( 100vh - 4.5rem)");
        // $(".lsph .scrollBox").css("height", "calc(100vh - 2.8rem)");
        if (isIphone) {
          // $(".phb .scrollBox").css("height", "calc( 100vh - 5rem)");
          // $(".lsph .scrollBox").css("height", "calc(100vh - 3.3rem)");
        }
      }
    }
  }
}